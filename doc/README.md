# Application TourGuide
L'objectif du projet 8 d'OpenClassRooms est d'une part de corriger les bugs existant et d'implementer 
des nouvelles fonctionnalités et d'autre part, de rendre l'application plus performante en reduisant les temps de reponse
de l'application 

## Spécification Technique
* Gradle 7.6
* Java 8
* Spring boot 2.1.6.RELEASE

## Commande
Avant toute chose, pour chaque commande il faut d'abord entrer dans le dossier ou se trouve le projet
```cd TourGuide```
* Lancer l'application ```gradle bootRun```
* Lancer les tests ```gradle test```

## Les EndPoints
* **GET** `` http://localhost:8080/``
* **GET** `` http://localhost:8080/getLocation``
* **GET** `` http://localhost:8080/getNearbyAttractions``
* **GET** `` http://localhost:8080/getRewards``
* **GET** `` http://localhost:8080/getAllCurrentLocations``
* **GET** `` http://localhost:8080/getTripDeals``
* **PUT** ``http://localhost:8080/preference``
