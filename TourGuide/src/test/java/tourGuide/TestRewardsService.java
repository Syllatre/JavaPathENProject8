package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.BeforeClass;
import org.junit.Test;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.user.UserReward;
import tourGuide.service.GpsUtilService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.model.user.User;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestRewardsService {
    @BeforeClass
    public static void init(){
        Locale.setDefault(new Locale("en", "US"));
    }

    @Test
    public void userGetRewards() throws Throwable {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        RewardCentral rewardCentral = new RewardCentral();

        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService,rewardCentral);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtil.getAttractions().get(0);
        System.out.println(gpsUtil.getAttractions().get(0));
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
        user.getVisitedLocations().forEach(System.out::println);
        tourGuideService.trackUserLocation(user).get();
        List<UserReward> userRewards = user.getUserRewards();
        tourGuideService.tracker.stopTracking();
        assertTrue(userRewards.size() == 1);
    }



    @Test
    public void nearAllAttractions() throws InterruptedException, ExecutionException {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        rewardsService.setProximityBuffer(Integer.MAX_VALUE);
        RewardCentral rewardCentral = new RewardCentral();

        InternalTestHelper.setInternalUserNumber(1);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService,rewardCentral);
        rewardsService.setProximityBuffer(10000);
        rewardsService.calculateRewards(tourGuideService.getAllUsers().get(0));
        List<UserReward> userRewards = tourGuideService.getUserRewards(tourGuideService.getAllUsers().get(0));
        tourGuideService.tracker.stopTracking();

        assertTrue(userRewards.size() != 0);
    }

    @Test
    public void calculateReward() throws InterruptedException, ExecutionException {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        RewardCentral rewardCentral = new RewardCentral();

        InternalTestHelper.setInternalUserNumber(1);


        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);
        rewardsService.setProximityBuffer(10000);
        User user = tourGuideService.getAllUsers().get(0);
        rewardsService.calculateRewards(user);
        System.out.println("------------------------------------------------------------");
        user.getUserRewards().forEach(r-> System.out.println(r.getRewardPoints()));
        System.out.println(user.getUserRewards().size());
        assertTrue( user.getUserRewards().size() != 0);

    }

    @Test
    public void isWithinAttractionProximity() {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        Attraction attraction = gpsUtil.getAttractions().get(0);
        assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
    }

}
