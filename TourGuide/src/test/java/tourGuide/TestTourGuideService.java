package tourGuide;


import gpsUtil.location.VisitedLocation;
import org.javamoney.moneta.Money;
import org.junit.BeforeClass;
import org.junit.Test;
import rewardCentral.RewardCentral;
import tourGuide.dto.UserAttractionDto;
import tourGuide.dto.UserPreferenceDto;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.user.User;
import tourGuide.service.GpsUtilService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tripPricer.Provider;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestTourGuideService {
    @BeforeClass
    public static void init() {
        Locale.setDefault(new Locale("en", "US"));
    }

    @Test
    public void getUserLocation() throws Throwable {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        tourGuideService.trackUserLocation(user).get();
        System.out.println(tourGuideService.trackUserLocation(user).get().userId);
        System.out.println(user.getUserId());
        tourGuideService.tracker.stopTracking();
        assertEquals(user.getLastVisitedLocation().userId, user.getUserId());
    }

    @Test
    public void addUser() throws InterruptedException {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        User retrivedUser = tourGuideService.getUser(user.getUserName());
        User retrivedUser2 = tourGuideService.getUser(user2.getUserName());

        tourGuideService.tracker.stopTracking();

        assertEquals(user, retrivedUser);
        assertEquals(user2, retrivedUser2);
    }

    @Test
    public void getAllUsers() throws InterruptedException {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        List<User> allUsers = tourGuideService.getAllUsers();

        tourGuideService.tracker.stopTracking();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(user2));
    }

    @Test
    public void trackUser() throws Throwable {
        RewardCentral rewardCentral = new RewardCentral();
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        CompletableFuture<VisitedLocation> visitedLocation = tourGuideService.trackUserLocation(user);

        tourGuideService.tracker.stopTracking();

        assertEquals(user.getUserId(), visitedLocation.get().userId);
    }

    @Test
    public void getUserPreference() throws InterruptedException {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);
        tourGuideService.resetMap();
        InternalTestHelper.setInternalUserNumber(1);
        tourGuideService.resetMap();
        User user = tourGuideService.getAllUsers().get(0);
        UserPreferenceDto userPreferenceDto =new UserPreferenceDto(50,100,500,20,3,2,3);
        tourGuideService.updatePreference(user.getUserName(), userPreferenceDto);
        tourGuideService.getUserPreferences(user.getUserName());

        assertEquals(userPreferenceDto.getLowerPricePoint(),tourGuideService.getUserPreferences(user.getUserName()).getLowerPricePoint());

    }


    @Test
    public void getNearbyAttractions() throws Throwable {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);
        InternalTestHelper.setInternalUserNumber(1);
        tourGuideService.resetMap();
        rewardsService.setProximityBuffer(10000);


        List<UserAttractionDto> attractionDtos = tourGuideService.getNearByAttractions(tourGuideService.getAllUsers().get(0).getUserName());

        tourGuideService.tracker.stopTracking();
        attractionDtos.forEach(a-> System.out.println(a.getRewardPoints()));
        assertEquals(5, attractionDtos.size());
        assertTrue(attractionDtos.get(4).getDistance() > attractionDtos.get(3).getDistance());
    }

    @Test
    public void getTripDeals() throws InterruptedException {
        GpsUtilService gpsUtil = new GpsUtilService();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(1);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService, rewardCentral);

        User user = tourGuideService.getAllUsers().get(0);
        CurrencyUnit currency = Monetary.getCurrency("USD");
        user.getUserPreferences().setLowerPricePoint(Money.of(400, currency));
        user.getUserPreferences().setHighPricePoint(Money.of(500, currency));

        List<Provider> providers = tourGuideService.getTripDeals(user);

        tourGuideService.tracker.stopTracking();
        providers.forEach(p -> {
            assertTrue(p.price > 400 && p.price < 500);
        });

    }

}
