package tourGuide.exception;

public class NotExistNameException extends RuntimeException{
    public NotExistNameException(final String message) {
        super((message));
    }
}
