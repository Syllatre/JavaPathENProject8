package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tourGuide.dto.UserPreferenceDto;
import tourGuide.exception.NotExistNameException;
import tourGuide.model.user.User;
import tourGuide.model.user.UserPreferences;
import tourGuide.service.TourGuideService;
import tripPricer.Provider;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class TourGuideController {

    @Autowired
    TourGuideService tourGuideService;

    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @RequestMapping("/getLocation")
    public String getLocation(@RequestParam @NotBlank final String userName) throws Throwable {
        VisitedLocation visitedLocation = (VisitedLocation) tourGuideService.getUserLocation(getUser(userName));
        return JsonStream.serialize(visitedLocation.location);

    }

    @RequestMapping("/getNearbyAttractions")
    public String getNearbyAttractions(@RequestParam @NotBlank final String userName) throws Throwable {
        return JsonStream.serialize(tourGuideService.getNearByAttractions(userName));
    }

    @RequestMapping("/getRewards")
    public String getRewards(@RequestParam @NotBlank final String userName) {
        return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
    }

    /**
     * Returns the latest location of all users in the form of a Map.
     *
     * @return a Map containing the user's identifier as the key and a Map containing the
     * longitude and latitude coordinates as the value.
     */
    @RequestMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {

        Map<String, Location> usersLastLocation = tourGuideService.getAllUsers().stream()
                .collect(Collectors.toMap(
                        user -> user.getUserId().toString(),
                        user -> new Location(user.getLastVisitedLocation().location.latitude,user.getLastVisitedLocation().location.longitude)
                ));
        return JsonStream.serialize(usersLastLocation);
    }

    @RequestMapping("/getTripDeals")
    public String getTripDeals(@RequestParam @NotBlank final String userName) {
        List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
        return JsonStream.serialize(providers);
    }

    /**
            * This method is used to update the user's preferences.
            *
            * @param userPreferencesDto an object containing the user's preferences.
            * @param userName the name of the user to update the preferences.
            * @return a serialized JSON string representation of the updated user preference.
            * @throws NotExistNameException if the user with the given name does not exist.
 */
    @PutMapping("/userPreference")
    public String updateUserPreference(@Valid @RequestBody final UserPreferenceDto userPreferencesDto, @RequestParam @NotBlank final String userName) {
        UserPreferenceDto userPreference = tourGuideService.updatePreference(userName, userPreferencesDto);
        return JsonStream.serialize(userPreference);
    }

    @RequestMapping("/userPreference")
    public String getUserPreference( @RequestParam @NotBlank final String userName) {
        UserPreferenceDto userPreference = tourGuideService.getUserPreferences(userName);
        return JsonStream.serialize(userPreference);
    }


    private User getUser(String userName) {
        return tourGuideService.getUser(userName);
    }


}