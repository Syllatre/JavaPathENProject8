package tourGuide.dto;


import com.jsoniter.annotation.JsonIgnore;
import gpsUtil.location.Location;

import java.util.UUID;

public class UserAttractionDto {
    private String attractionName;
    private Location attractionLocation;
    private Location userLocation;
    private int rewardPoints;
    private double distance;
    @JsonIgnore
    private UUID attractionId;

    public UserAttractionDto(String attractionName, Location attractionLocation, Location userLocation, int rewardPoints, double distance,UUID attractionId) {
        this.attractionName = attractionName;
        this.attractionLocation = attractionLocation;
        this.userLocation = userLocation;
        this.rewardPoints = rewardPoints;
        this.distance = distance;
        this.attractionId = attractionId;
    }

    public UUID getAttractionId() {
        return attractionId;
    }

    public void setAttractionId(UUID attractionId) {
        this.attractionId = attractionId;
    }

    public String getAttractionName() {
        return attractionName;
    }

    public void setAttractionName(String attractionName) {
        this.attractionName = attractionName;
    }

    public Location getAttractionLocation() {
        return attractionLocation;
    }

    public void setAttractionLocation(Location attractionLocation) {
        this.attractionLocation = attractionLocation;
    }

    public Location getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(Location userLocation) {
        this.userLocation = userLocation;
    }

    public int getRewardPoints() {
        return rewardPoints;
    }

    public void setRewardPoints(int rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "UserAttractionDto{" +
                "attractionName='" + attractionName + '\'' +
                ", attractionLocation=" + attractionLocation +
                ", userLocation=" + userLocation +
                ", rewardPoints=" + rewardPoints +
                ", distance=" + distance +
                ", attractionId=" + attractionId +
                '}';
    }
}
