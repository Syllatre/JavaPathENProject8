package tourGuide.service;


import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.dto.UserAttractionDto;
import tourGuide.dto.UserPreferenceDto;
import tourGuide.exception.NotExistNameException;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.user.User;
import tourGuide.model.user.UserPreferences;
import tourGuide.model.user.UserReward;
import tourGuide.tracker.Tracker;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TourGuideService {
    private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);
    private final GpsUtilService gpsUtil;

    private final RewardCentral rewardCentral;
    private final RewardsService rewardsService;
    private final TripPricer tripPricer = new TripPricer();

    ThreadPoolExecutor executorService = new ThreadPoolExecutor(
            5,
            50,
            1,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(8),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.CallerRunsPolicy()
    );

    public final Tracker tracker;


    boolean testMode = true;

    public TourGuideService(GpsUtilService gpsUtil, RewardsService rewardsService, RewardCentral rewardCentral) throws InterruptedException {
        this.gpsUtil = gpsUtil;
        this.rewardsService = rewardsService;
        this.rewardCentral = rewardCentral;

        if (testMode) {
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            initializeInternalUsers();
            logger.debug("Finished initializing users");
        }
        tracker = new Tracker(this);
        addShutDownHook();
    }

    public List<UserReward> getUserRewards(User user) {
        return user.getUserRewards();
    }

    public Object getUserLocation(User user) throws ExecutionException, InterruptedException {
        return (user.getVisitedLocations().size() > 0) ?
                user.getLastVisitedLocation() :
                trackUserLocation(user).get();
    }

    public User getUser(String userName) throws NotExistNameException {
        User user = internalUserMap.get(userName);
        if (user == null) {
            throw new NotExistNameException("user not exist");
        }
        return user;
    }

    public List<User> getAllUsers() {
        return internalUserMap.values().stream().collect(Collectors.toList());
    }

    public void addUser(User user) {
        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }

    public void resetMap() {
        if (internalUserMap.size() > 0) {
            internalUserMap.clear();
        }
        initializeInternalUsers();
    }

    public List<Provider> getTripDeals(User user) {
        int cumulativeRewardPoints =
                user.getUserRewards().stream()
                        .mapToInt(i -> i.getRewardPoints())
                        .sum();

        List<Provider> providers =
                tripPricer.getPrice(
                        tripPricerApiKey,
                        user.getUserId(),
                        user.getUserPreferences().getNumberOfAdults(),
                        user.getUserPreferences().getNumberOfChildren(),
                        user.getUserPreferences().getTripDuration(),
                        cumulativeRewardPoints
                );
        // Updates the Provider list based on user preferences
        user.setTripDeals(providers);
        return user.getTripDeals().stream().filter(a -> a.price >= user.getUserPreferences().getLowerPricePoint().getNumber().doubleValue() && a.price <= user.getUserPreferences().getHighPricePoint().getNumber().doubleValue()).collect(Collectors.toList());
    }


    public CompletableFuture<VisitedLocation> trackUserLocation(User user) {
        return gpsUtil.getUserLocation(user).thenApply(visitedLocation -> {
            user.addToVisitedLocations(visitedLocation);
            try {
                rewardsService.calculateRewards(user);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return visitedLocation;
        });
    }


    /**
     * Retourne une liste des attractions les plus proches d'un utilisateur donné, classées par distance.
     * La liste contient les informations sur l'attraction, les récompenses associées et la distance à parcourir.
     *
     * @param userName nom de l'utilisateur pour lequel on veut trouver les attractions proches
     * @return une liste de 5 attractions les plus proches de l'utilisateur, classées par distance.
     */
    public List<UserAttractionDto> getNearByAttractions(String userName) throws Throwable {
        User user = getUser(userName);
        VisitedLocation visitedLocation = (VisitedLocation) getUserLocation(user);
        List<UserAttractionDto> attractions = gpsUtil.getAttractions().stream()
                .map(a -> {
                    double distance = rewardsService.distanceBetweenAttractionAndVisitedLocation(a, visitedLocation.location);
                    return new UserAttractionDto(a.attractionName, new Location(a.latitude, a.longitude), new Location(user.getLastVisitedLocation().location.latitude, user.getLastVisitedLocation().location.longitude), 0, distance, a.attractionId);
                }).sorted(Comparator.comparing(UserAttractionDto::getDistance))
                .limit(5).collect(Collectors.toList());
        //TODO changer rewardCentral ppar rewardService
        attractions.forEach(r -> rewardCentral.getAttractionRewardPoints(r.getAttractionId(), user.getUserId()));

        return attractions;
    }


    public UserPreferenceDto updatePreference(String userName, UserPreferenceDto userPreferenceDto) {
        User user = getUser(userName);

        UserPreferences preferences = user.getUserPreferences();
        preferences.setAttractionProximity(userPreferenceDto.getAttractionProximity());
        preferences.setHighPricePoint(Money.of(userPreferenceDto.getHighPricePoint(), preferences.getCurrency()));
        preferences.setLowerPricePoint(Money.of(userPreferenceDto.getLowerPricePoint(), preferences.getCurrency()));
        preferences.setTripDuration(userPreferenceDto.getTripDuration());
        preferences.setTicketQuantity(userPreferenceDto.getTicketQuantity());
        preferences.setNumberOfAdults(userPreferenceDto.getNumberOfAdults());
        preferences.setNumberOfChildren(userPreferenceDto.getNumberOfChildren());
        System.out.println(preferences);

        return userPreferenceDto;
    }

    public UserPreferenceDto getUserPreferences(String userName) {
        UserPreferences up = getUser(userName).getUserPreferences();
        return new UserPreferenceDto(up.getAttractionProximity(), up.getLowerPricePoint().getNumber().intValue(), up.getHighPricePoint().getNumber().intValue(), up.getTripDuration(), up.getTripDuration(), up.getNumberOfAdults(), up.getNumberOfAdults());
    }

    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                tracker.stopTracking();
            }
        });
    }

    /**********************************************************************************
     *
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String tripPricerApiKey = "test-server-api-key";
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private final Map<String, User> internalUserMap = new HashMap<>();

    private void initializeInternalUsers() {
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
        });
        logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
    }

    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
        });
    }

    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
