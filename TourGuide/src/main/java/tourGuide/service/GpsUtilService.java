package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.model.user.User;

import java.util.List;
import java.util.concurrent.*;


public class GpsUtilService {
    private GpsUtil gpsUtil;

    private final Logger logger = LoggerFactory.getLogger(RewardsService.class);

    public GpsUtilService() {
        this.gpsUtil = new GpsUtil();
    }

    ThreadPoolExecutor executorService = new ThreadPoolExecutor(
            5,
            50,
            1,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(8),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.CallerRunsPolicy()
    );

    public CompletableFuture<VisitedLocation> getUserLocation(User user){
        logger.info(Thread.currentThread().getName());
        return CompletableFuture.supplyAsync(()-> gpsUtil.getUserLocation(user.getUserId()),executorService);
    }
    public List<Attraction> getAttractions (){
        return gpsUtil.getAttractions();
    }
}
