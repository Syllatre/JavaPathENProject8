package tourGuide.service;


import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.stream.Collectors;

@Service

public class RewardsService {
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;
    private final Logger logger = LoggerFactory.getLogger(RewardsService.class);

    // proximity in miles
    private final int defaultProximityBuffer = 10;
    private int proximityBuffer = defaultProximityBuffer;
    private final int attractionProximityRange = 200;
    private final GpsUtilService gpsUtil;
    private final RewardCentral rewardsCentral;


   ExecutorService executorService;



    public RewardsService(GpsUtilService gpsUtil, RewardCentral rewardCentral) {
        this.gpsUtil = gpsUtil;
        this.rewardsCentral = rewardCentral;
        this.executorService = Executors.newFixedThreadPool(1000);
        addShutDownHook();
        Runtime.getRuntime().addShutdownHook(new Thread(executorService::shutdownNow));

    }

    public void setProximityBuffer(int proximityBuffer) {
        this.proximityBuffer = proximityBuffer;
    }

    public void setDefaultProximityBuffer() {
        proximityBuffer = defaultProximityBuffer;
    }

    public void calculateRewards(User user) throws ExecutionException, InterruptedException {
        List<VisitedLocation> userLocations = user.getVisitedLocations();
        List<Attraction> attractions = gpsUtil.getAttractions();

        for(VisitedLocation visitedLocation : userLocations) {
            for(Attraction attraction : attractions) {
                if(user.getUserRewards().stream().filter(r -> r.attraction.attractionName.equals(attraction.attractionName)).count() == 0) {
                    if(nearAttraction(visitedLocation, attraction)) {
                        extracted(user, visitedLocation, attraction).get();
                    }
                }
            }
        }
    }

    private CompletableFuture<Void> extracted(User user, VisitedLocation visitedLocation, Attraction attraction) {
        logger.info("extracted");
        return CompletableFuture.supplyAsync(()->  getRewardPoints(attraction, user),executorService)
                .thenAccept(rewardPoint -> user.addUserReward(new UserReward(visitedLocation, attraction,rewardPoint)));

    }
    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                executorService.shutdownNow();
            }
        });
    }

//    public void calculateRewards(User user) {
//            List<VisitedLocation> userLocations = user.getVisitedLocations();
//            List<Attraction> allAttractions = gpsUtil.getAttractions();
//            List<UserReward> userRewards = user.getUserRewards();
//
//            // Get the attractions that the user has not visited yet
//            List<Attraction> attractionsToVisit = new CopyOnWriteArrayList<>();
//            for (Attraction attraction : allAttractions) {
//                if (userRewards.stream().noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName))) {
//                    attractionsToVisit.add(attraction);
//                }
//            }
//
//            List<CompletableFuture<UserReward>> futures = new ArrayList<>();
//
//            // Calculate the rewards for each location and attraction in a separate thread
//            for (VisitedLocation visitedLocation : userLocations) {
//                for  (Attraction attraction : attractionsToVisit) {
//                    CompletableFuture<UserReward> future = CompletableFuture.supplyAsync(() -> {
//                        if (nearAttraction(visitedLocation, attraction)) {
//
//                            return new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user));
//                        }
//                        return null;
//                    }, executorService);
//                    futures.add(future);
//                }
//            }
//
//            // Collect the rewards calculated by each future
//            List<UserReward> newRewards = futures.stream()
//                    .map(CompletableFuture::join)
//                    .filter(Objects::nonNull)
//                    .collect(Collectors.toList());
//
//            userRewards.addAll(newRewards);
//
//
//         //Sort the rewards by their reward points in descending order
//        userRewards.sort((r1, r2) -> Integer.compare(r2.getRewardPoints(), r1.getRewardPoints()));
//    }


//    public void calculateRewards(User user) {
//        List<VisitedLocation> userLocations = user.getVisitedLocations();
//        List<Attraction> allAttractions = gpsUtil.getAttractions();
//        List<UserReward> userRewards = user.getUserRewards();
//        List<CompletableFuture<UserReward>> futures = new ArrayList<>();
//
//        // Get the attractions that the user has not visited yet
//        List<Attraction> attractionsToVisit = new CopyOnWriteArrayList<>();
//        for (Attraction attraction : allAttractions) {
//            if (userRewards.stream().noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName))) {
//                attractionsToVisit.add(attraction);
//            }
//        }
//
//        for (VisitedLocation visitedLocation : userLocations) {
//            for (Attraction attraction : attractionsToVisit) {
//                if (nearAttraction(visitedLocation, attraction)) {
//                    userRewards.add(new UserReward(visitedLocation, attraction, 0));
//                }
//            }
//        }
//        futures = userRewards.stream().map(u -> {
//            return CompletableFuture.supplyAsync(() -> {
//               logger.info(Thread.currentThread().getName());
//                UserReward userReward = new UserReward(u.visitedLocation, u.attraction, getRewardPoints(u.attraction, user));
//                return userReward;
//            });
//        }).collect(Collectors.toList());
//        System.out.println("------------------------------------------------------------------------------");
//        System.out.println(futures.size());
//        System.out.println("------------------------------------------------------------------------------");
////        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).join();
//        List<UserReward> newRewards = futures.stream()
//                .map(CompletableFuture::join)
//                .collect(Collectors.toList());
//
//        userRewards.addAll(newRewards);
//
//    }

    public double distanceBetweenAttractionAndVisitedLocation(Attraction attraction, Location location) {
        return getDistance(attraction, location);
    }

    public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
        return !(getDistance(attraction, location) > attractionProximityRange);
    }

    private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
        return !(getDistance(attraction, visitedLocation.location) > proximityBuffer);
    }


    public int getRewardPoints(Attraction attraction, User user) {
        return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
    }

    public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }

}
